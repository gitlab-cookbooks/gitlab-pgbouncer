# Cookbook:: gitlab-pgbouncer
# Recipe:: default
# License:: MIT
#
# Copyright:: 2018, GitLab Inc.

secrets_hash  = node['gitlab-pgbouncer']['secrets']
version       = node['gitlab-pgbouncer']['version']
postgres_user = node['gitlab-pgbouncer']['user']
log_file      = node['gitlab-pgbouncer']['logfile']
secrets       = get_secrets(secrets_hash['backend'], secrets_hash['path'], secrets_hash['key'])

executable_path = "#{node['pgbouncer']['source']['install_dir']}/bin/pgbouncer"

node.default['pgbouncer']['source']['url'] = "https://pgbouncer.github.io/downloads/files/#{version}/pgbouncer-#{version}.tar.gz"

if log_file.nil? || log_file.empty?
  log_file = '/var/log/gitlab/pgbouncer/pgbouncer.log'
end

apt_update do
  action :update
end

%w(libssl-dev pkg-config).each do |pkg|
  package pkg
end

include_recipe 'pgbouncer-service::install_from_source'

if node['gitlab-pgbouncer']['upgrade']
  edit_resource(:execute, 'build pgbouncer') do
    # Overwrite https://github.com/kigster/cookbook-pgbouncer-service/blob/a62858038661382bbeff29c9aebb8cec3051124b/recipes/install_from_source.rb#L45
    # so we can compile and install the new version
    not_if.clear
    not_if "#{executable_path} -V | grep #{version}"
  end

  ruby_block 're-run build' do
    block {}
    notifies :run, 'execute[build pgbouncer]', :immediately
  end
end

user postgres_user do
  not_if { node['etc']['passwd'].key?(postgres_user) }
end

log_directory = File.dirname(log_file)

directory node['gitlab-pgbouncer']['data_directory'] do
  owner postgres_user
  mode '0700'
  recursive true
end

directory log_directory do
  owner 'syslog'
  group 'syslog'
  recursive true
end

if (pgb_console_user = PgbouncerHelper.pgb_console_user(node, secrets))
  postgresql_version = node['gitlab-pgbouncer']['postgresql']['version']

  # Adapted from the postgresql cookbook
  package 'apt-transport-https'

  apt_values = node['gitlab-pgbouncer']['postgresql']['apt']
  apt_repository 'postgresql' do
    uri apt_values['uri']
    components apt_values['components']
    distribution apt_values['distribution']
    key apt_values['key']
    cache_rebuild true
  end

  package "postgresql-client-#{postgresql_version}"
end

pgbouncer_atttributes = node['gitlab-pgbouncer'].to_hash
service_names = []
pgbouncer_atttributes.delete('listen_ports').map.with_index do |listen_port, index|
  # For backwards compatibility we don't number the first service
  if index == 0
    service_name = 'pgbouncer'
    console_name = 'pgb-console'
  else
    service_name = "pgbouncer-#{index}"
    console_name = "pgb-console-#{index}"
  end

  service_names << service_name
  data_dir    = pgbouncer_atttributes['data_directory']
  config_path = File.join(data_dir, "#{service_name}.ini")
  pidfile     = File.join(data_dir, "pgbouncer-#{index}.pid")

  template config_path do
    source 'pgbouncer.ini.erb'
    variables pgbouncer_atttributes.merge(listen_port: listen_port, pidfile: pidfile)
    owner postgres_user
    group node['gitlab-pgbouncer']['group']
    mode '0600'
    notifies :reload, "poise_service[#{service_name}]", :delayed
  end

  poise_service service_name do
    command "#{executable_path} #{config_path}"
    reload_signal 'HUP'
    user postgres_user
    options :systemd, template: 'pgbouncer.systemd.erb'
    restart_on_update false
  end

  template "/usr/local/bin/#{console_name}" do
    source 'pgb-console.erb'
    variables(
      postgres_user: postgres_user,
      port: listen_port,
      psql_command: node['gitlab-pgbouncer']['psql_command'],
      user: pgb_console_user
    )
    mode '0777'
  end
end

PgbouncerHelper.each_removed_pgbouncer_suffixes(node, :systemd_service) do |suffix|
  service_name = "pgbouncer#{suffix}"
  console_name = "pgb-console#{suffix}"

  data_dir    = pgbouncer_atttributes['data_directory']
  config_path = File.join(data_dir, "#{service_name}.ini")

  poise_service service_name do
    action [:stop, :disable]
  end

  file config_path do
    action :delete
  end

  file "/usr/local/bin/#{console_name}" do
    action :delete
  end
end

template "#{node['gitlab-pgbouncer']['data_directory']}/pg_auth" do
  source 'pg_auth.erb'
  variables(users: secrets['gitlab-pgbouncer']['users'])

  service_names.each do |service_name|
    notifies :reload, "poise_service[#{service_name}]", :delayed
  end
end

template node['gitlab-pgbouncer']['databases_ini'] do
  source "#{File.basename(name)}.erb"
  variables(databases: node['gitlab-pgbouncer']['databases'].to_hash)
  owner postgres_user
  group node['gitlab-pgbouncer']['group']
  mode '0600'

  service_names.each do |service_name|
    notifies :reload, "poise_service[#{service_name}]", :delayed
  end
end

service 'rsyslog' do
  supports [:restart]
end

template '/etc/rsyslog.d/60-pgbouncer.conf' do
  source 'rsyslog.conf.erb'
  variables(
    program_name: node['gitlab-pgbouncer']['syslog_ident'],
    log_path: log_file
  )
  notifies :restart, 'service[rsyslog]', :delayed
end

include_recipe 'logrotate::default'

logrotate_app 'pgbouncer' do
  path log_file
  options %w(missingok compress delaycompress notifempty)
  rotate 7
  frequency 'daily'
end

include_recipe 'gitlab-pgbouncer::monitoring'
