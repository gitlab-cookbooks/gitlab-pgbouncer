# Cookbook:: gitlab-pgbouncer
# Recipe:: ha_leader
# License:: MIT
#
# Copyright:: 2019, GitLab Inc.

consul_executable_path = node['gitlab-pgbouncer']['ha_leader']['consul_executable_path']
lock_prefix            = node['gitlab-pgbouncer']['ha_leader']['lock_prefix']
web_server_cmd         = node['gitlab-pgbouncer']['ha_leader']['web_server_cmd']
leaders_count          = node['gitlab-pgbouncer']['ha_leader']['leaders_count']
service_name           = 'pgbouncer-leader-check'
working_dir            = '/tmp/pgbouncer-leader-check'

directory working_dir

poise_service service_name do
  command "#{consul_executable_path} lock -n=#{leaders_count} #{lock_prefix} #{web_server_cmd}"
  directory working_dir
  options :systemd, template: 'pgbouncer-leader-check.systemd.erb'
  notifies :run, "execute[enable #{service_name}]", :immediately
end

# We need to run `systemctl reenable` explicitly for RequiredBy
# to take effect. Neither `poise_service` nor `service` does it
# for us.
execute "enable #{service_name}" do
  command "systemctl reenable #{service_name}.service"
  action :nothing
end
