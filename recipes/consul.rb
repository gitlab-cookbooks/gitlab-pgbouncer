# Cookbook:: gitlab-pgbouncer
# Recipe:: consul
# License:: MIT
#
# Copyright:: 2021, GitLab Inc.

secrets_hash                      = node['gitlab-pgbouncer']['secrets']
secrets                           = get_secrets(secrets_hash['backend'], secrets_hash['path'], secrets_hash['key'])
use_db_connection_as_health_check = node['gitlab-pgbouncer']['consul']['db_connection_health_check']['enabled']
consul_linux_user                 = node['consul']['service_user']
consul_home_directory             = "/home/#{consul_linux_user}"
consul_postgresql_username        = node['gitlab-pgbouncer']['consul']['db_connection_health_check']['postgresql_user']
consul_postgresql_database        = node['gitlab-pgbouncer']['consul']['db_connection_health_check']['postgresql_database']
consul_postgresql_password        = secrets['gitlab-pgbouncer']['consul']['db_connection_health_check']['postgresql_password']

# Make sure Consul's home directory exists
directory consul_home_directory do
  owner consul_linux_user
  only_if { use_db_connection_as_health_check }
end

file "#{consul_home_directory}/.pgpass" do
  content "*:*:*:*:#{consul_postgresql_password}"
  owner consul_linux_user
  sensitive true
  mode '0600'
  only_if { use_db_connection_as_health_check }
end

service 'consul' do
  supports [:reload]
end

consul_service_name = node['gitlab-pgbouncer']['consul']['service_name']
check_interval = node['gitlab-pgbouncer']['consul']['check_interval']
port_service_name_overrides = node['gitlab-pgbouncer']['consul']['port_service_name_overrides'] || {}

node['gitlab-pgbouncer']['listen_ports'].each.with_index do |listen_port, port_index|
  # For backwards compatibility we don't number the first service
  if port_index == 0
    id                   = consul_service_name
    systemd_service_name = 'pgbouncer'
  else
    id                   = "#{consul_service_name}-#{port_index}"
    systemd_service_name = "pgbouncer-#{port_index}"
  end

  check_args = if use_db_connection_as_health_check
                 %W(psql -h localhost -p #{listen_port} -U #{consul_postgresql_username} -d #{consul_postgresql_database} -c select;)
               else
                 ['systemctl', 'status', systemd_service_name]
               end

  service_id = "#{consul_service_name}-#{port_index}"

  service_parameters = {
    id: service_id,
    # Allow overriding so some ports have a different service_name. We default to
    # the global service_name if there is no override provided. We only
    # override this value so the logic to cleanup deleted ports still works. We
    # also need to `to_s` as the configuration comes from JSON and you can't
    # have integer keys in JSON.
    name: port_service_name_overrides[listen_port.to_s] || consul_service_name,
    port: listen_port,
    checks: [
      {
        args: check_args,
        interval: check_interval,
      },
    ].concat(node['gitlab-pgbouncer']['consul']['extra_checks']),
  }

  consul_definition id do
    type 'service'
    parameters(service_parameters)
    notifies :reload, 'service[consul]', :delayed
  end
end

PgbouncerHelper.each_removed_pgbouncer_suffixes(node, :consul_service) do |suffix|
  consul_definition "#{consul_service_name}#{suffix}" do
    type 'services'
    action :delete
    notifies :reload, 'service[consul]', :delayed
  end
end
