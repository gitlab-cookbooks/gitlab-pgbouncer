pgbouncer_exporter_instances = []

data_dir = node['gitlab-pgbouncer']['data_directory']

node['gitlab-pgbouncer']['listen_ports'].each.with_index do |listen_port, index|
  log_dir_suffix = "_#{index}" if index > 0

  pgbouncer_exporter_instances << {
    log_dir: "/var/log/prometheus/pgbouncer_exporter#{log_dir_suffix}",
    flags: {
      'pgBouncer.connectionString' => "'user=pgbouncer dbname=pgbouncer sslmode=disable port=#{listen_port} host=#{data_dir}'",
      'pgBouncer.pid-file' => File.join(data_dir, "pgbouncer-#{index}.pid"),
      'web.listen-address' => "0.0.0.0:#{9188 + index}",
    },
  }
end

node.default['pgbouncer_exporter']['instances'] = pgbouncer_exporter_instances

include_recipe 'gitlab-exporters::process_exporter'

config_file_regexp = File.join(data_dir, '(?P<configfile>[^ ]+)')

node.default['process_exporter']['config'] = {
  process_names: [
    {
      name: 'pgbouncer.{{.Matches.configfile}}',
      comm: [
        'pgbouncer',
      ],
      cmdline: [
        "^/usr/local/bin/pgbouncer(.*)#{config_file_regexp}$",
      ],
    },
  ],
}

include_recipe 'gitlab-exporters::pgbouncer_exporter'
