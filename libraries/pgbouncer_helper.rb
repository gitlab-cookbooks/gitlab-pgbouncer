module PgbouncerHelper
  def self.pgb_console_user(node, secrets)
    authenticatable_users = secrets['gitlab-pgbouncer']['users'].keys
    admin_users = node['gitlab-pgbouncer']['admin_users']
    suitable_users = admin_users & authenticatable_users
    return suitable_users.first if suitable_users.any?

    stats_users = node['gitlab-pgbouncer']['stats_users']
    suitable_users = stats_users & authenticatable_users
    suitable_users.first
  end

  def self.each_removed_pgbouncer_suffixes(node, resource_type)
    listen_ports = node['gitlab-pgbouncer']['listen_ports']

    consul_service_name = node['gitlab-pgbouncer']['consul']['service_name']

    services = case resource_type
               when :systemd_service
                 Dir['/etc/systemd/system/pgbouncer*.service']
               when :consul_service
                 Dir["/etc/consul/conf.d/#{consul_service_name}*"]
               else
                 []
               end

    (services.length - 1).downto(listen_ports.length).each do |index|
      # For backwards compatibility we don't number the first service
      yield (index == 0 ? '' : "-#{index}")
    end
  end
end
