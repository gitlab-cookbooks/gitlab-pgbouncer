# Cookbook:: gitlab-pgbouncer
# Spec:: default
#
# Copyright:: 2018, GitLab B.V., MIT.

require 'spec_helper'

describe 'gitlab-pgbouncer::default' do
  let(:chef_run) do
    ChefSpec::ServerRunner.new do |node|
      node.normal['etc']['passwd'] = {}
      node.normal['gitlab-pgbouncer']['databases'] = {
        gitlabhq_production: {
          host: 'localhost',
          dbname: 'gitlabhq_production',
        },
      }
    end.converge(described_recipe)
  end

  before do
    mock_secrets_path = 'spec/fixtures/secrets.json'
    secrets           = JSON.parse(File.read(mock_secrets_path))

    allow_any_instance_of(Chef::Recipe).to receive(:get_secrets)
      .with('dummy', { 'path' => 'gitlab-gstg-secrets/gitlab-pgbouncer', 'item' => 'gstg.enc' }, 'ring' => 'gitlab-secrets', 'key' => 'gstg', 'location' => 'global')
      .and_return(secrets)
  end

  it 'installs libssl-dev' do
    expect(chef_run).to install_package('libssl-dev')
  end

  describe 'PostgreSQL user' do
    context 'when the user exists' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new do |node|
          node.normal['etc']['passwd']['gitlab-psql'] = {}
        end.converge(described_recipe)
      end

      it 'does not create the user' do
        expect(chef_run).not_to create_user('gitlab-psql')
      end
    end

    context 'when the user does not exist' do
      it 'creates PostgreSQL user' do
        expect(chef_run).to create_user('gitlab-psql')
      end
    end
  end

  describe 'pgbouncer log directory' do
    context 'logfile attribute is empty' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new do |node|
          node.normal['gitlab-pgbouncer']['logfile'] = ''
          node.normal['etc']['passwd'] = {}
        end.converge(described_recipe)
      end

      it 'creates /var/log/gitlab/pgbouncer directory' do
        expect(chef_run).to create_directory('/var/log/gitlab/pgbouncer').with(owner: 'syslog', group: 'syslog', recursive: true)
      end
    end

    context 'logfile attribute exists' do
      it 'creates the directory' do
        expect(chef_run).to create_directory('/var/log/gitlab/pgbouncer').with(owner: 'syslog', group: 'syslog', recursive: true)
      end
    end
  end

  it 'creates pgbouncer data directory' do
    expect(chef_run).to create_directory('/var/opt/gitlab/pgbouncer').with(owner: 'gitlab-psql', mode: '0700', recursive: true)
  end

  describe 'APT repository' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new do |node|
        node.normal['gitlab-pgbouncer']['logfile'] = ''
        node.normal['etc']['passwd'] = {}
        node.normal['gitlab-pgbouncer']['postgresql']['version'] = '13'
      end.converge(described_recipe)
    end

    it 'creates a postgresql APT repository' do
      expect(chef_run).to add_apt_repository('postgresql').with(
        uri: 'https://download.postgresql.org/pub/repos/apt/',
        components: %w(main 13),
        distribution: "#{chef_run.node['lsb']['codename']}-pgdg",
        key: ['https://download.postgresql.org/pub/repos/apt/ACCC4CF8.asc']
      )
    end
  end

  it 'creates pg_auth' do
    auth_file = '/var/opt/gitlab/pgbouncer/pg_auth'

    expect(chef_run).to render_file(auth_file).with_content('"amar" "hunter1"')
    expect(chef_run.template(auth_file)).to notify('poise_service[pgbouncer]').to(:reload).delayed
  end

  it 'creates pgbouncer.ini' do
    config_path    = '/var/opt/gitlab/pgbouncer/pgbouncer.ini'
    config_content = File.read('spec/fixtures/pgbouncer.ini')

    expect(chef_run).to create_template(config_path).with(owner: 'gitlab-psql', group: 'gitlab-psql', mode: '0600')
    expect(chef_run).to render_file(config_path).with_content(config_content)
    expect(chef_run.template(config_path)).to notify('poise_service[pgbouncer]').to(:reload).delayed
  end

  it 'creates databases.ini' do
    databases_path    = '/var/opt/gitlab/pgbouncer/databases.ini'
    databases_content = File.read('spec/fixtures/databases.ini')

    expect(chef_run).to create_template(databases_path).with(owner: 'gitlab-psql', group: 'gitlab-psql', mode: '0600')
    expect(chef_run).to render_file(databases_path).with_content(databases_content)
    expect(chef_run.template(databases_path)).to notify('poise_service[pgbouncer]').to(:reload).delayed
  end

  it 'creates pgb-console' do
    pgb_console_path    = '/usr/local/bin/pgb-console'
    pgb_console_content = <<-STR
#!/bin/sh

if [ "$(id -n -u)" = "gitlab-psql" ] ; then
  privilege_drop=''
else
  privilege_drop="-u gitlab-psql"
fi

cd /tmp; exec chpst ${privilege_drop} -U gitlab-psql gitlab-psql -p 6432 -U pgbouncer -d pgbouncer "$@"
    STR

    expect(chef_run).to create_template(pgb_console_path).with(mode: '0777')
    expect(chef_run).to render_file(pgb_console_path).with_content(pgb_console_content)
  end

  it 'creates pgbouncer rsyslog config' do
    config_path = '/etc/rsyslog.d/60-pgbouncer.conf'

    expect(chef_run).to render_file(config_path).with_content("if $programname == 'pgbouncer' then /var/log/gitlab/pgbouncer/pgbouncer.log;pgbouncer_svlogd_format")
    expect(chef_run.template(config_path)).to notify('service[rsyslog]').to(:restart).delayed
  end

  it 'rotates pgbouncer logs' do
    expect(chef_run).to enable_logrotate_app('pgbouncer').with(
      path: '/var/log/gitlab/pgbouncer/pgbouncer.log',
      options: %w(missingok compress delaycompress notifempty),
      rotate: 7,
      frequency: 'daily'
    )
  end

  context 'listen port(s) removed' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new do |node|
        node.normal['etc']['passwd'] = {}
        node.normal['gitlab-pgbouncer']['listen_ports'] = [6543, 6544]
        node.normal['gitlab-pgbouncer']['databases'] = {
          gitlabhq_production: {
            host: 'localhost',
            dbname: 'gitlabhq_production',
          },
        }
      end.converge(described_recipe)
    end

    before do
      allow(Dir).to receive(:[]).and_call_original
      allow(Dir).to receive(:[]).with('/etc/systemd/system/pgbouncer*.service').and_return(%w(pgbouncer.service pgbouncer-1.service pgbouncer-2.service))
    end

    it 'removes relevant service files' do
      expect(chef_run).to delete_file('/var/opt/gitlab/pgbouncer/pgbouncer-2.ini')
      expect(chef_run).to delete_file('/usr/local/bin/pgb-console-2')
    end

    it 'stops and disables relevant service' do
      expect(chef_run).to stop_poise_service('pgbouncer-2')
      expect(chef_run).to disable_poise_service('pgbouncer-2')
    end
  end

  describe 'monitoring recipe' do
    describe 'setting process_exporter attributes' do
      context 'empty config attribute' do
        default_attributes['process_exporter']['config'] = {}

        let(:chef_run) do
          ChefSpec::ServerRunner.new(chef_runner_options) do |node|
            node.normal['etc']['passwd'] = {}
          end.converge(described_recipe)
        end

        it 'adds a process definition for pgbouncer' do
          process_names = chef_run.node['process_exporter']['config']['process_names']

          expect(process_names[0]['name']).to eq('pgbouncer.{{.Matches.configfile}}')
        end
      end

      context 'pre-populated config attribute' do
        default_attributes['process_exporter']['config']['process_names'] = [{ 'exe' => ['osqueryd'] }]

        let(:chef_run) do
          ChefSpec::ServerRunner.new(chef_runner_options) do |node|
            node.normal['etc']['passwd'] = {}
          end.converge(described_recipe)
        end

        it 'appends a process definition for pgbouncer' do
          process_names = chef_run.node['process_exporter']['config']['process_names']

          expect(process_names.count).to eq(2)
          expect(process_names[0]['name']).to eq('pgbouncer.{{.Matches.configfile}}')
          expect(process_names[0]['cmdline'][0]).to eq('^/usr/local/bin/pgbouncer(.*)/var/opt/gitlab/pgbouncer/(?P<configfile>[^ ]+)$')
          expect(process_names[1]['exe']).to eq(['osqueryd'])
        end
      end
    end
  end

  describe 'consul recipe' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new do |node|
        node.normal['gitlab-pgbouncer']['consul']['service_name'] = 'my-replica'
        node.normal['gitlab-pgbouncer']['listen_ports'] = [6543, 6544]
      end.converge('gitlab-pgbouncer::consul')
    end

    it 'adds a consul_definition for each port of the service' do
      expect(chef_run.consul_definition('my-replica').type).to eq('service')
      expect(chef_run.consul_definition('my-replica').parameters[:name]).to eq('my-replica')
      expect(chef_run.consul_definition('my-replica').parameters[:id]).to eq('my-replica-0')
      expect(chef_run.consul_definition('my-replica').parameters[:port]).to eq(6543)
      expect(chef_run.consul_definition('my-replica-1').type).to eq('service')
      expect(chef_run.consul_definition('my-replica-1').parameters[:name]).to eq('my-replica')
      expect(chef_run.consul_definition('my-replica-1').parameters[:id]).to eq('my-replica-1')
      expect(chef_run.consul_definition('my-replica-1').parameters[:port]).to eq(6544)
    end

    context 'with port_service_name_overrides' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new do |node|
          node.normal['gitlab-pgbouncer']['consul']['service_name'] = 'my-replica'
          node.normal['gitlab-pgbouncer']['listen_ports'] = [6543, 6544, 9999, 9998]
          node.normal['gitlab-pgbouncer']['consul']['port_service_name_overrides'] = { '9999' => 'my-other-replica', '9998' => 'my-another-replica' }
        end.converge('gitlab-pgbouncer::consul')
      end

      it 'adds a consul_definition for each port with service overidden by port_service_name_overrides' do
        expect(chef_run.consul_definition('my-replica').type).to eq('service')
        expect(chef_run.consul_definition('my-replica').parameters[:name]).to eq('my-replica')
        expect(chef_run.consul_definition('my-replica').parameters[:id]).to eq('my-replica-0')
        expect(chef_run.consul_definition('my-replica').parameters[:port]).to eq(6543)

        expect(chef_run.consul_definition('my-replica-1').type).to eq('service')
        expect(chef_run.consul_definition('my-replica-1').parameters[:name]).to eq('my-replica')
        expect(chef_run.consul_definition('my-replica-1').parameters[:id]).to eq('my-replica-1')
        expect(chef_run.consul_definition('my-replica-1').parameters[:port]).to eq(6544)

        expect(chef_run.consul_definition('my-replica-2').type).to eq('service')
        expect(chef_run.consul_definition('my-replica-2').parameters[:name]).to eq('my-other-replica')
        expect(chef_run.consul_definition('my-replica-2').parameters[:id]).to eq('my-replica-2')
        expect(chef_run.consul_definition('my-replica-2').parameters[:port]).to eq(9999)

        expect(chef_run.consul_definition('my-replica-3').type).to eq('service')
        expect(chef_run.consul_definition('my-replica-3').parameters[:name]).to eq('my-another-replica')
        expect(chef_run.consul_definition('my-replica-3').parameters[:id]).to eq('my-replica-3')
        expect(chef_run.consul_definition('my-replica-3').parameters[:port]).to eq(9998)
      end
    end

    context 'listen port(s) removed' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new do |node|
          node.normal['gitlab-pgbouncer']['listen_ports'] = [6543, 6544]
        end.converge('gitlab-pgbouncer::consul')
      end

      before do
        allow(Dir).to receive(:[]).and_call_original
        allow(Dir).to receive(:[]).with('/etc/consul/conf.d/pgbouncer*').and_return(%w(pgbouncer pgbouncer-1 pgbouncer-2))
      end

      it 'removes relevant Consul service' do
        expect(chef_run).to delete_consul_definition('pgbouncer-2')
        expect(chef_run.consul_definition('pgbouncer-2')).to notify('service[consul]').to(:reload).delayed
      end
    end
  end
end
