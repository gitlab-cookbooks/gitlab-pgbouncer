default['gitlab-pgbouncer']['consul']['service_name'] = 'pgbouncer'
default['gitlab-pgbouncer']['consul']['check_interval'] = '10s'
default['gitlab-pgbouncer']['consul']['extra_checks'] = []
default['gitlab-pgbouncer']['consul']['db_connection_health_check']['enabled'] = false
default['gitlab-pgbouncer']['consul']['db_connection_health_check']['postgresql_user'] = 'consul'
default['gitlab-pgbouncer']['consul']['db_connection_health_check']['postgresql_password'] = 'in vault'
default['gitlab-pgbouncer']['consul']['db_connection_health_check']['postgresql_database'] = 'gitlab'
