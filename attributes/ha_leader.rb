default['gitlab-pgbouncer']['ha_leader']['consul_executable_path'] = '/usr/local/bin/consul'
default['gitlab-pgbouncer']['ha_leader']['lock_prefix'] = 'service/pgbouncer-ha-cluster'
default['gitlab-pgbouncer']['ha_leader']['web_server_cmd'] = '/usr/bin/python -m SimpleHTTPServer 8010'
default['gitlab-pgbouncer']['ha_leader']['leaders_count'] = 1
