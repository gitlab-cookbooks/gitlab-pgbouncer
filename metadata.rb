name             'gitlab-pgbouncer'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'Installs and configures pgbouncer for GitLab'
version          '1.3.3'
chef_version     '>= 12.1'
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab-pgbouncer/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab-pgbouncer'

supports 'ubuntu', '= 16.04'

# Please specify dependencies with version pin:
depends 'pgbouncer-service', '~> 0.2.0'
depends 'poise-service', '~> 1.5.2'
depends 'logrotate', '~> 2.2.0'
depends 'gitlab_secrets'
depends 'gitlab-exporters'
